import { Injectable } from '@angular/core';
import { AngularFireAuth } from 'angularfire2/auth';
import { Router } from '@angular/router';

const identifier = 'token';
const email_user = 'email';

@Injectable()
export class AuthenticationService {

  public token: string;

  constructor(private angularAuth: AngularFireAuth, private router: Router) {
    this.setUp();
  }

  setUp() {
    this.token = this.getTokenFromLS();
    console.log('Listening to observer');
    this.angularAuth.authState.subscribe((firebaseUser) => {
      console.log('USER', firebaseUser);
      if (firebaseUser) {
        localStorage.setItem(identifier, firebaseUser.uid);
        localStorage.setItem(email_user, firebaseUser.email);
        this.token = firebaseUser.uid;
        console.log(this.token);
      } else {
        localStorage.removeItem(identifier);
        localStorage.removeItem(email_user);
        this.token = null;
      }
    })
  }

  getTokenFromLS(): string {
    return localStorage.getItem(identifier);
  }

  signUp(email, password) {
    return this.angularAuth.auth.signInWithEmailAndPassword(email, password);
  }

  logOut() {
    return this.angularAuth.auth.signOut()
    .then(() => {
      this.token = null;
      this.router.navigate(['']);
    })
  }
}
