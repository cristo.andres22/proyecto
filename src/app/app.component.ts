import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from './services/authentication.service';
import { Router } from '@angular/router';

@Component({
  // tslint:disable-next-line
  selector: 'body',
  template: '<router-outlet></router-outlet>'
})
export class AppComponent implements OnInit {

  email: string;
  password: string;

  constructor(private authentication: AuthenticationService,
              private router: Router) {
                if (!this.authentication.token) {
                  this.router.navigate(['']);
                }
              }

  ngOnInit() {
    if (!this.authentication.token) {
      this.router.navigate(['']);
    }
  }
}
