import { ToasterConfig } from 'angular2-toaster';

export const toasterConfig: ToasterConfig =
  new ToasterConfig({
    tapToDismiss: true,
    timeout: 5000
  });
