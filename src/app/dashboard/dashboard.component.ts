import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from '../services/authentication.service';
import { Router } from '@angular/router';
import { DashboardService } from './dashboard.service';

@Component({
  templateUrl: 'dashboard.component.html'
})
export class DashboardComponent implements OnInit {
  user: any;
  amount = 0;
  balance = 0;

  constructor(private authentication: AuthenticationService,
    private router: Router,
    private dashboardServices: DashboardService) { }

  ngOnInit() {
    if (!this.authentication.token) {
      this.router.navigate(['']);
    }
    const token = localStorage.getItem('token');
    this.dashboardServices.findId(token).subscribe(
      data => {
        this.user = data.json();
        console.log(this.user);
        this.user.sales.forEach(element => {
          this.amount += +element.amount;
          this.balance += +element.balance;
          console.log(this.amount)
        });
      }, error => {
        console.log(error);
      }
    )
  }

}
