import {Injectable} from '@angular/core';
import {Http, Response} from '@angular/http';
import {Observable} from 'rxjs/Rx';

@Injectable()
export class DashboardService {

  constructor(private http: Http) {

  }

  all(): Observable<Response> {
    return this.http.get('https://expressjs-sequelize.herokuapp.com/user')
    .catch(() => Observable.throw('Algo salio malo'));
  }

  findId(uuid: string): Observable<Response> {
    return this.http.get(`https://expressjs-sequelize.herokuapp.com/user/${uuid}/sale`)
    .catch(() => Observable.throw('Algo salio malo'));
  }
}
