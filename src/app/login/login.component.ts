import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from '../services/authentication.service';
import { ToasterConfig, ToasterService } from 'angular2-toaster';
import { toasterConfig } from '../app.config';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  email: string;
  password: string;
  public toasterConfig: ToasterConfig = toasterConfig;

  constructor(private authentication: AuthenticationService,
              private router: Router,
              private _toasterService: ToasterService) { }

  ngOnInit() {
    const token = localStorage.getItem('token');
    if (token) {
      this.router.navigate(['/home/dashboard']);
    } else {
      this.router.navigate(['']);
    }
  }

  signUp() {
    this.authentication.signUp(this.email, this.password)
    .then((data) => {
      console.log(data);
      this.router.navigate(['/home/dashboard']);
    }).catch((err) => {
      console.log('ERROR', err);
      this._toasterService.pop('error', 'ERROR AL INGRESAR', 'usuario / contraseña invalidos');
    })
  }

  createAccount() {
    /*this.angularFire.auth.createUserWithEmailAndPassword(this.email, this.password)
    .then((data) => {
      console.log(data);
    }).catch((err) => {
      console.log(err);
    })*/
  }
}
