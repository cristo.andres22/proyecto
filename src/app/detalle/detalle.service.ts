import {Injectable} from '@angular/core';
import {Http, Response} from '@angular/http';
import {Observable} from 'rxjs/Rx';

@Injectable()
export class DetalleService {

  constructor(private http: Http) {

  }

  all(): Observable<Response> {
    return this.http.get('https://expressjs-sequelize.herokuapp.com/user/sale')
    .catch(() => Observable.throw('Algo salio malo'));
  }

  deleteSale(id: number): Observable<Response> {
    return this.http.delete(`https://expressjs-sequelize.herokuapp.com/user/sale/${id}`)
    .catch(() => Observable.throw('Algo salio malo'));
  }
}
