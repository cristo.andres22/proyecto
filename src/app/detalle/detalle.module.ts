import { NgModule } from '@angular/core';
import { ChartsModule } from 'ng2-charts/ng2-charts';
import { CommonModule } from '@angular/common';

import { DetalleComponent } from './detalle.component';
import { DetalleRoutingModule } from './detalle-routing.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DetalleService } from './detalle.service';
import { ModalModule } from 'ngx-bootstrap/modal';
import { ToasterModule, ToasterService } from 'angular2-toaster/angular2-toaster';

@NgModule({
  imports: [
    DetalleRoutingModule,
    ChartsModule,
    CommonModule,
    FormsModule,
    ToasterModule,
    ModalModule.forRoot(),
  ],
  declarations: [ DetalleComponent ],
  providers: [DetalleService, ToasterService]
})
export class DetalleModule { }
