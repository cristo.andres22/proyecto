import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from '../services/authentication.service';
import { Router } from '@angular/router';
import { DetalleService } from './detalle.service';
import { ModalDirective } from 'ngx-bootstrap/modal/modal.component';
import { ToasterConfig, ToasterService } from 'angular2-toaster';
import { toasterConfig } from '../app.config';

@Component({
  selector: 'app-detalle',
  templateUrl: './detalle.component.html',
  styleUrls: ['./detalle.component.scss']
})
export class DetalleComponent implements OnInit {

  user: any;
  amount = 0;
  balance = 0;
  saleId: any;
  public toasterConfig: ToasterConfig = toasterConfig;

  constructor(private authentication: AuthenticationService,
    private router: Router,
    private detalleService: DetalleService,
    private _toasterService: ToasterService) { }

  ngOnInit() {
    if (!this.authentication.token) {
      this.router.navigate(['']);
    }
    const token = localStorage.getItem('token');
    this.detalleService.all().subscribe(
      data => {
        this.user = data.json();
        console.log(this.user);
      }, error => {
        console.log(error);
      }
    )
  }

  deleteSale(id: number) {
    this.detalleService.deleteSale(id).subscribe(
      data => {
        this._toasterService.pop('success', 'DEUDA ELIMINADA', 'deuda eliminada exitosamente');
      }, error => {
        console.log(error);
        console.log('ERROR', error);
        this._toasterService.pop('error', 'ERROR AL ELIMINAR', 'error al eliminar deuda');
      }
    )
  }

  modalDelete(id: number) {
    this.saleId = id;
  }
}
